# Yuwen_IDS721_mini4



## Project Description

This is the mini-project 4 for IDS721 -- Containerize a Rust Actix Web Service. For my Actix app, I build a page to display the current time.


## Steps

### Creat a new rust project using Cargo. [Cargo Documentation](https://web.mit.edu/rust-lang_v1.25/arch/amd64_ubuntu1404/share/doc/rust/html/cargo/guide/creating-a-new-project.html)


```
cargo new <YOUR_PROJECT_NAME>

```

### Integrate with Actix [Actix Documentation](https://actix.rs/docs/getting-started/)

1. Modify main.rs to write your own function to create the app.
```
//defining a function that return current time
async fn time() -> impl Responder {
    let time = chrono::Local::now();
    HttpResponse::Ok().body(format!("The current time is: {}", time))
}
```


2. Add corresponding dependencies into Cargo.toml
```
actix-web = "4"
chrono = "0.4.33"
```

### Generate Dockerfile [Rust Docker Documentation](https://hub.docker.com/_/rust)
1. modify your own working directory

```WORKDIR /usr/src/<YOUR_PROJECT_NAME>```

2. add a expose port
```
# Expose port 8080 to the outside world (used by Actix-Web)
EXPOSE 8080
```

### Test Application before containerization
```
cargo build

cargo run
```

### Build Docker Image
```
docker build . -t <YOUR_IMAGE_NAME>
```
![docker_image](rust-actix-app/imgs/images.png)

### Run Docker Container on your exposed port
```
docker run --name <NAME_YOUR_CONTAINER_OPTIONAL> -p 8080:8080 <YOUR_IMAGE_NAME>
```
![docker_container](rust-actix-app/imgs/containers.png)

### Access export port on running containers
![local_host](rust-actix-app/imgs/local_host.png)