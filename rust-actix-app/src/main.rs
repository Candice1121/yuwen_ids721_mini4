//importing the actix-web crate
use actix_web::{web, App, HttpServer, Responder, HttpResponse};
use chrono;

//defining a function that return current time
async fn time() -> impl Responder {
    let time = chrono::Local::now();
    HttpResponse::Ok().body(format!("The current time is: {}", time))
}

//mofidy main to render a index.html page
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(
            web::scope("/app")
                .route("/index.html", web::get().to(time))
        )
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
